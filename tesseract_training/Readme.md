############## "For TESSERACT setup please follow the instructions given below" ####### 
 git clone --depth 1  https://github.com/tesseract-ocr/tesseract.git tesseract
cd tesseract
./autogen.sh
./configure --enable-debug
make
sudo make install
sudo ldconfig 
build training tools if you like:
make training
sudo make training-install 
## For Leptonica installation, follow below instructions.
Download .tar.gtesseractz form cloud from fl-datasets
Go to this link   http://gwang-cv.github.io/2017/08/25/ubuntu16.04+Tesseract4.0/
And follow the steps accordingly in that link
In that process we are downloading Leptonica 1.79.0
git clone --recursive https://github.com/DanBloomberg/leptonica
cd leptonica
./autogen.sh
./configure
./make-for-auto
sudo make
sudo make install	
####### *** "TRAININIG_PROCESS "*** #######
We can download the required fonts from Google. Here is the link "https://www.myfonts.com/WhatTheFont"
For traininig, What are the fonts we need should keep those in fonts path :"/usr/local/share/fonts/"
keep the model "eng.traineddata" and yo1.go script in this path "/usr/local/share/tessdata/"
Next come to this path "tesseract-4.1.0-rc1/train/" and create one empty folder for storing training pages 
make changes in the script auto.old.sh 

### For Loading Pages ###
Give the font name in fontlist argument and also Give the directory name at output_dir train/which is you are created in the path "tesseract-4.1.0-rc1/train/"
sudo src/training/tesstrain.sh --fonts_dir /usr/local/share/fonts --fontlist  'Digital-7 mono' 'Vitali slab fat, Regular' 'Falling sky, Bold' --lang eng --linedata_only --langdata_dir langdata_lstm  --training_text ./digits_printed.txt  --tessdata_dir tessdata --save_box_tiff --maxpages 500000 --output_dir train/Directory_name
echo "#####################################################################################################################pages loading completed"

### For Training ###
give the directory name at train_listfile ./train/"directory name"
sudo src/training/lstmtraining --continue_from ./best_lstm/eng.lstm --model_output new_out/calorie/ --traineddata tessdata/eng.traineddata --train_listfile ./train/DEMO7/eng.training_files.txt --max_iterations 1000 --learning_rate=10e-3
echo "######################################################################################training completed"

### For testing ###
go to this path '/usr/local/share/tessdata/'
run this command 'for f in calorie/*; do go run yo1.go --lang "${f%.traineddata}";done'
After testing, all csv files are present in this path '/usr/local/share/tessdata/csv_reports/calorie'
Next, go to this path : '/usr/local/share/tessdata/csv_reports/calorie'
run compare_csv.py script, You will get all models accuracy, green bucket percentage and green bucket accuracy.
#########################     TESTING_PROCESS_IN_LOCAL    ##########################
For testing we use 'Go' language. so, install Go using this link 'https://tecadmin.net/install-go-on-ubuntu/'
For testing in local go to this path '/usr/local/share/tessdata/' which model you want to test that model should be converted to eng.traineddata
Next open OCR_Gosseract folder . in this all test images are must be present in test_images folder
For testing, you need to install some dependencies. open the terminal where you have OCR_Gosseract folder and run these commands one by one
sudo apt-get install libjpeg-dev
sudo apt-get update -y
sudo apt-get install -y libtiff-dev
sudo apt-get install libpng-dev
sudo apt-get install -y libgif-dev
Next run these commands one by one for testing purpose: 
export PATH=$PATH:/usr/local/go/bin
ulimit -Hn 50000
ulimit -Sn 45000
go run char_conf_new.go
after testing you will get csv_reports...
