torchvision==0.15.0
torch
pytorch-quantization
six==1.16.0
typing_extensions==4.4.0
pillow==9.4.0
numpy==1.23.5
certifi==2022.12.7
