CUDA_VISIBLE_DEVICES=0 python demo.py \
  --height 32 \
  --width 224 \
  --voc_type ALLCASES_SYMBOLS \
  --arch ResNet_ASTER \
  --with_lstm \
  --max_len 100 \
  --STN_ON \
  --beam_width 5 \
  --tps_inputsize 32 64 \
  --tps_outputsize 32 100 \
  --tps_margins 0.05 0.05 \
  --stn_activation none \
  --num_control_points 20 \
  --resume /home/vishwam/mountpoint/bhanu/AADHAR_ASTER_PYTORCH/aster.pytorch/logs/baseline_aster/model_best.pth.tar \
  --image_path /home/vishwam/mountpoint/bhanu/AADHAR_ASTER_PYTORCH/DE_8kgold/0a1a2f6e728f7cb82a9cf60171a30ef0_6.png
  