#sudo rm /usr/local/share/tessdata/calorie*
#sudo rm /usr/local/share/tessdata/csv_reports/*.csv
#cd /home/pravalika/tesseract-4.1.1/
sudo src/training/tesstrain.sh --fonts_dir fonts_aadhar/ --fontlist 'Arial' 'Arial, Bold' 'Calibri, Bold' --lang eng --linedata_only --langdata_dir langdata_lstm  --training_text ./half_25julytoteng.training_text  --tessdata_dir tessdata --save_box_tiff --maxpages 50 --output_dir aadhar_text/
echo "#####################################################################################################################pages loading completed"
sudo src/training/lstmtraining --continue_from ./best_lstm/eng.lstm --model_output new_out/ --traineddata tessdata/eng.traineddata --train_listfile ./train/task/eng.training_files.txt --max_iterations 100000 --learning_rate=10e-3
echo "######################################################################################training completed"
#sudo mkdir output_checkpoints
#sudo cp ./*.checkpoint ./output_checkpoints
for f in new_out/*.checkpoint; do sudo src/training/lstmtraining --stop_training --continue_from $f --traineddata tessdata/eng.traineddata --model_output $f;done
for f in new_out/*.checkpoint; do sudo mv -- "$f" "${f%.checkpoint}.traineddata";done
echo "################################################################################converted checkpoints to models"
#sudo mv new_out/*.traineddata /usr/local/share/tessdata/calorie/
sudo rm new_out/_checkpoint
echo "###################################################################models moved to automodels folder in tessdata"
#cd /usr/local/share/tessdata/
#for f  in  calorie/*; do go run yo1.go ;done
#for f in calorie/*; do go run yo1.go --lang "${f%.traineddata}";done
#cd csv_reports
#python3 mergecsv.py
#python3 comparecsv.py
#sudo nano Accuracy.txt

